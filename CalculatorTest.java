package cz.fel.cvut.ts1;

import cz.cvut.fel.ts1.Calculator;
import cz.cvut.fel.ts1.Main;
import cz.cvut.fel.ts1.Illia1;
import org.junit.jupiter.api.Assertions;
import org.testng.annotations.Test;


public class CalculatorTest {
    @Test
    public void addTest(){
        Calculator calculator = new Calculator();
        int num1 = 1, num2 = 2;
        int expectation = 3;

        int result = calculator.add(num1, num2);

        Assertions.assertEquals(expectation, result);
    }
    @Test
    public void substractTest(){
        Calculator calculator = new Calculator();
        int num1 = 1, num2 = 2;
        int expectation = -1;

        int result = calculator.subtract(num1, num2);

        Assertions.assertEquals(expectation, result);
    }
    @Test
    public void multiplyTest(){
        Calculator calculator = new Calculator();
        int num1 = 3, num2 = 5;
        int expectation = 15;

        long result = calculator.multiply(num1, num2);

        Assertions.assertEquals(expectation, result);
    }
    @Test
    public void divideTest() throws Exception {
        Calculator calculator = new Calculator();
        int num1 = 10, num2 = 2;
        double expectation = 5;

        double result = calculator.divide(num1, num2);

        Assertions.assertEquals(expectation, result);
    }

    @Test( expectedExceptions = Exception.class)
    public void testDivisionByZero() throws Exception {
        Illia1 calculator = new Illia1();
        calculator.divide(5, 0);
    }

}


